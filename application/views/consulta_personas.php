<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Consulta Personas</title>
    <link rel="stylesheet" href="http://localhost/PruebaTecnica/application/assets/style.css" media="screen" title="no title">
  </head>
  <body>
    <section id ="main_container">
      <div class="head container">
        <h1>Filtro</h1>
        <hr>
        <div class="form container">
          <form class="" action="http://localhost/PruebaTecnica/index.php/consultar" method="post">
            <label>cedula</label>
            <input type="text" name="cedula" id="cedula" value=<?= $this->session->flashdata('cedula');?> >
            <label>nombre</label>
            <input type="text" name="nombre" id="nombre" value=<?= $this->session->flashdata('nombre');?> >
            <input type="submit" value="filtrar">
            <input type="button" id=limpiar value="limpiar">
          </form>
        </div>
      </div>
      <div class="result container">
            <?php if (isset($data)) {
              if (count($data) > 0) {
                echo "<table>
                      <thead>
                        <tr>
                          <td>ID</td>
                          <td>Cedula</td>
                          <td>Nombre</td>
                          <td>Email</td>
                        </tr>
                      </thead>
                      <tbody>";
                foreach ($data as $key => $value) {
                  echo "<tr>";
                  foreach ($value as $key => $value) {
                    echo '<td>'.$value.'</td>';
                  }
                  echo "</tr>";
                }
              }else{
                echo "<tr><td>No se encontró ninguna coincidencia</td></tr>";
              }
            }
            echo "</tbody></table>";
            ?>
      </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://localhost/PruebaTecnica/application/assets/script.js"></script>
  </body>
</html>
