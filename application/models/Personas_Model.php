<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personas_Model extends CI_Model {

  public function consultarNombre($nombre)
	{
    $table = $this->db->get_where('personas',array('nombre' => $nombre));
    return $table->result_array();
	}
  public function consultarCedula($cedula)
	{
    $table = $this->db->get_where('personas',array('cedula' => $cedula));
    return $table->result_array();
	}
  public function consultarNombreCedula($nombre,$cedula)
	{
    $table = $this->db->get_where('personas',array('nombre' => $nombre,'cedula'=>$cedula));
    return $table->result_array();
	}
  public function consultarTodos()
	{
    $table = $this->db->get('personas');
    return $table->result_array();
	}
}
