<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class personas_controller extends CI_Controller {

  public function index()
	{
    $this->load->library('session');
		$this->load->view('consulta_personas');
	}

  public function consultar()
	{
    $this->load->model('Personas_Model');
    $nombre = $this->input->post('nombre');
    $cedula = $this->input->post('cedula');

    $this->load->library('session');
    $this->session->set_flashdata('nombre',$nombre);
    $this->session->set_flashdata('cedula',$cedula);

    if ($nombre == '' && $cedula == '')
    {
      $data['data'] = $this->Personas_Model->consultarTodos();
    }
    else if ($cedula == '')
    {
      $data['data'] = $this->Personas_Model->consultarNombre($nombre);
    }
    else if($nombre == '')
    {
      $data['data'] = $this->Personas_Model->consultarCedula($cedula);
    }
    else
    {
      $data['data'] = $this->Personas_Model->consultarNombreCedula($nombre,$cedula);
    }

    $this->load->view('consulta_personas',$data);
	}
}
